<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ro_RO">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="34"/>
        <source>opensnitch-qt</source>
        <translation>opensnitch-qt</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="679"/>
        <source>from this executable</source>
        <translation>din acest executabil</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="684"/>
        <source>from this command line</source>
        <translation>din această linie de comandă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="689"/>
        <source>this destination port</source>
        <translation>acest port de destinație</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="694"/>
        <source>this user</source>
        <translation>acest utilizator</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="699"/>
        <source>this destination ip</source>
        <translation>această adresă IP de destinație</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="842"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="723"/>
        <source>once</source>
        <translation>o dată</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="728"/>
        <source>30s</source>
        <translation>30s</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="733"/>
        <source>5m</source>
        <translation>5m</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="738"/>
        <source>15m</source>
        <translation>15m</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="743"/>
        <source>30m</source>
        <translation>30m</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="748"/>
        <source>1h</source>
        <translation>1o</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="753"/>
        <source>until reboot</source>
        <translation>până la repornire</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="758"/>
        <source>forever</source>
        <translation>mereu</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="784"/>
        <source>Deny</source>
        <translation>Refuză</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="813"/>
        <source>Allow</source>
        <translation>Permite</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="299"/>
        <source>User ID</source>
        <translation>ID utilizator</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="333"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Executed from&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Executat din&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="630"/>
        <source>TextLabel</source>
        <translation>EtichetăText</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="426"/>
        <source>Source IP</source>
        <translation>Adresă IP sursă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="449"/>
        <source>Process ID</source>
        <translation>ID proces</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="582"/>
        <source>Destination IP</source>
        <translation>Adresă IP destinație</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/prompt.ui" line="605"/>
        <source>Dst Port</source>
        <translation>Port destinație</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="14"/>
        <source>Preferences</source>
        <translation>Preferințe</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="417"/>
        <source>UI</source>
        <translation>Interfață utilizator</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="361"/>
        <source>Show advanced view by default</source>
        <translation>Arată implicit vizualizarea avansată</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="695"/>
        <source>once</source>
        <translation>o dată</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="226"/>
        <source>30s</source>
        <translation>30s</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="231"/>
        <source>5m</source>
        <translation>5m</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="236"/>
        <source>15m</source>
        <translation>15m</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="241"/>
        <source>30m</source>
        <translation>30m</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="246"/>
        <source>1h</source>
        <translation>1o</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="251"/>
        <source>until reboot</source>
        <translation>până la repornire</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="256"/>
        <source>forever</source>
        <translation>mereu</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="526"/>
        <source>Action</source>
        <translation>Acțiune</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="325"/>
        <source>Default target</source>
        <translation>Țintă implicită</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="377"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, the pop-ups will be displayed with the advanced view active.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Dacă este bifată, ferestrele de notificare care apar vor fi afișate cu vizualizarea avansată activă.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="737"/>
        <source>deny</source>
        <translation>refuză</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="746"/>
        <source>allow</source>
        <translation>permite</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="290"/>
        <source>by executable</source>
        <translation>după executabil</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="295"/>
        <source>by command line</source>
        <translation>după linia de comandă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="300"/>
        <source>by destination port</source>
        <translation>după portul de destinație</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="305"/>
        <source>by destination ip</source>
        <translation>după adresa IP de destinație</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="310"/>
        <source>by user id</source>
        <translation>după identificatorul utilizatorului</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="186"/>
        <source>center</source>
        <translation>centru</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="191"/>
        <source>top right</source>
        <translation>sus la dreapta</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="196"/>
        <source>bottom right</source>
        <translation>jos la dreapta</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="201"/>
        <source>top left</source>
        <translation>sus la stânga</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="206"/>
        <source>bottom left</source>
        <translation>jos la stânga</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="342"/>
        <source>Pop-up default duration</source>
        <translation>Durată implicită fereastră de notificare</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="345"/>
        <source>Duration</source>
        <translation>Durată</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="270"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default when a new pop-up appears, in its simplest form, you&apos;ll be able to filter connections or applications by one property of the connection (executable, port, IP, etc).&lt;/p&gt;&lt;p&gt;With these options, you can choose multiple fields to filter connections for.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default when a new pop-up appears, in its simplest form, you&apos;ll be able to filter connections or applications by one property of the connection (executable, port, IP, etc).&lt;/p&gt;&lt;p&gt;With these options, you can choose multiple fields to filter connections for.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="273"/>
        <source>Filter connections also by:</source>
        <translation>Filtrează conexiunile și după:</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="97"/>
        <source>User ID</source>
        <translation>ID utilizator</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="113"/>
        <source>Destination port</source>
        <translation>Port destinație</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="129"/>
        <source>Destination IP</source>
        <translation>Adresă IP destinație</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="406"/>
        <source>Disable pop-ups, only display an alert</source>
        <translation>Dezactivează ferestrele de notificare, arată doar o alertă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="396"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This timeout is the countdown you see when a pop-up dialog is shown.&lt;/p&gt;&lt;p&gt;If the pop-up is not answered, the default options will be applied.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This timeout is the countdown you see when a pop-up dialog is shown.&lt;/p&gt;&lt;p&gt;If the pop-up is not answered, the default options will be applied.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="399"/>
        <source>Default timeout</source>
        <translation>Durată implicită pentru alegere</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="540"/>
        <source>Nodes</source>
        <translation>Noduri</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="546"/>
        <source>Process monitor method</source>
        <translation>Metodă monitorizare procese</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="563"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Log file to write logs.&lt;br/&gt;&lt;/p&gt;&lt;p&gt;/dev/stdout will print logs to the standard output.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fișierul jurnal unde să se scrie jurnalizări.&lt;br/&gt;&lt;/p&gt;&lt;p&gt;/dev/stdout va tipări jurnalizările pe ieșirea standard.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="566"/>
        <source>Log file</source>
        <translation>Fișier de jurnalizare</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="580"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The default duration will take place when there&apos;s no UI connected.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Durata implicită va fi folosită când nu este conectată nicio interfață grafică.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="583"/>
        <source>Default duration</source>
        <translation>Durată implicită</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="596"/>
        <source>Apply configuration to all nodes</source>
        <translation>Aplică configurația la toate nodurile</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="619"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The default action will take place when there&apos;s no UI connected.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Acțiunea implicită va fi folosită când nu este conectată nicio interfață grafică.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="638"/>
        <source>HostName</source>
        <translation>NumeGazdă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="700"/>
        <source>until restart</source>
        <translation>până la repornire</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="705"/>
        <source>always</source>
        <translation>întotdeauna</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="713"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Address of the node.&lt;/p&gt;&lt;p&gt;Default: unix:///tmp/osui.sock (unix:// is mandatory if it&apos;s a Unix socket)&lt;/p&gt;&lt;p&gt;It can also be an IP address with the port: 127.0.0.1:50051&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Address of the node.&lt;/p&gt;&lt;p&gt;Default: unix:///tmp/osui.sock (unix:// is mandatory if it&apos;s a Unix socket)&lt;/p&gt;&lt;p&gt;It can also be an IP address with the port: 127.0.0.1:50051&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="716"/>
        <source>Address</source>
        <translation>Adresă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="764"/>
        <source>Version</source>
        <translation>Versiune</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="815"/>
        <source>unix:///tmp/osui.sock</source>
        <translation>unix:///tmp/osui.sock</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="827"/>
        <source>/var/log/opensnitchd.log</source>
        <translation>/var/log/opensnitchd.log</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="832"/>
        <source>/dev/stdout</source>
        <translation>/dev/stdout</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="856"/>
        <source>Default log level</source>
        <translation>Nivel implicit de jurnalizare</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="871"/>
        <source>Database</source>
        <translation>Bază de date</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="918"/>
        <source>Database type</source>
        <translation>Tip bază de date</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="925"/>
        <source>Select</source>
        <translation>Selectare</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="946"/>
        <source>In memory</source>
        <translation>În memorie</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="951"/>
        <source>File</source>
        <translation>Fișier</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="1008"/>
        <source>Close</source>
        <translation>Închide</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="1019"/>
        <source>Apply</source>
        <translation>Aplică</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="1030"/>
        <source>Save</source>
        <translation>Salvează</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="358"/>
        <source>The advanced view allows you to easily select multiple fields to filter connections</source>
        <translation>Vizualizarea avansată vă permite să selectați cu ușurință câmpuri multiple pentru a filtra conexiunile</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="126"/>
        <source>If checked, this field will be selected when a pop-up is displayed</source>
        <translation>Dacă este bifată, acest câmp va fi selectat când o fereastră de notificare este afișată</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="166"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pop-up default action.&lt;/p&gt;&lt;p&gt;When a new outgoing connection is about to be established, this action will be selected by default, so if the timeout fires, this is the option that will be applied.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;While a pop-up is asking the user to allow or deny a connection:&lt;/p&gt;&lt;p&gt;1. new outgoing connections are denied.&lt;/p&gt;&lt;p&gt;2. known connections are allowed or denied based on the rules defined by the user.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Pop-up default action.&lt;/p&gt;&lt;p&gt;When a new outgoing connection is about to be established, this action will be selected by default, so if the timeout fires, this is the option that will be applied.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;While a pop-up is asking the user to allow or deny a connection:&lt;/p&gt;&lt;p&gt;1. new outgoing connections are denied.&lt;/p&gt;&lt;p&gt;2. known connections are allowed or denied based on the rules defined by the user.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="622"/>
        <source>Default action when the GUI is disconnected</source>
        <translation>Acțiune implicită când interfața grafică cu utilizatorul este deconectată</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="726"/>
        <source>Debug invalid connections</source>
        <translation>Depanează conexiunile nevalide</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="39"/>
        <source>Pop-ups</source>
        <translation>Ferestre de notificare</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="80"/>
        <source>Default options</source>
        <translation>Opțiuni implicite</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="332"/>
        <source>Default position on screen</source>
        <translation>Compozziția implicită pe ecran</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="437"/>
        <source>any temporary rules</source>
        <translation>oricare regulă temporară</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="450"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When this option is selected, the rules of the selected duration won&apos;t be added to the list of temporary rules in the GUI.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;Temporary rules will still be valid, and you can use them when prompted to allow/deny a new connection.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When this option is selected, the rules of the selected duration won&apos;t be added to the list of temporary rules in the GUI.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;Temporary rules will still be valid, and you can use them when prompted to allow/deny a new connection.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="453"/>
        <source>Don&apos;t save rules of duration</source>
        <translation>Nu salva regulile de durată</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="466"/>
        <source>Time</source>
        <translation>Timp</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="476"/>
        <source>Destination</source>
        <translation>Destinație</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="486"/>
        <source>Protocol</source>
        <translation>Protocol</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="496"/>
        <source>Process</source>
        <translation>Proces</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="506"/>
        <source>Rule</source>
        <translation>Regulă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="516"/>
        <source>Node</source>
        <translation>Nod</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="723"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, opensnitch will prompt you to allow or deny connections that don&apos;t have an asocciated PID, due to several reasons, mostly due to bad state connections.&lt;/p&gt;&lt;p&gt;The pop-up dialog will only contain information about the network connection.&lt;/p&gt;&lt;p&gt;There&apos;re some scenarios where these are valid connections though, like when establishing a VPN using wireguard.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, opensnitch will prompt you to allow or deny connections that don&apos;t have an asocciated PID, due to several reasons, mostly due to bad state connections.&lt;/p&gt;&lt;p&gt;The pop-up dialog will only contain information about the network connection.&lt;/p&gt;&lt;p&gt;There&apos;re some scenarios where these are valid connections though, like when establishing a VPN using wireguard.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/preferences.ui" line="460"/>
        <source>Events tab columns</source>
        <translation>Coloane etichete evenimente</translation>
    </message>
</context>
<context>
    <name>ProcessDetailsDialog</name>
    <message>
        <location filename="../../../opensnitch/res/process_details.ui" line="14"/>
        <source>Process details</source>
        <translation>Detalii proces</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/process_details.ui" line="61"/>
        <source>loading...</source>
        <translation>Se încarcă...</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/process_details.ui" line="81"/>
        <source>CWD: loading...</source>
        <translation>CWD: loading...</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/process_details.ui" line="93"/>
        <source>mem stats: loading...</source>
        <translation>Statistici memorie: se încarcă...</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/process_details.ui" line="121"/>
        <source>Status</source>
        <translation>Stare</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/process_details.ui" line="135"/>
        <source>Open files</source>
        <translation>Deschidere fișiere</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/process_details.ui" line="149"/>
        <source>I/O Statistics</source>
        <translation>Statistici intrare/ieșire</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/process_details.ui" line="163"/>
        <source>Memory mapped files</source>
        <translation>Fișiere cartografiate în memorie</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/process_details.ui" line="177"/>
        <source>Stack</source>
        <translation>Stivă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/process_details.ui" line="191"/>
        <source>Environment variables</source>
        <translation>Variabile de mediu</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/process_details.ui" line="210"/>
        <source>Application pids</source>
        <translation>Identificatori procese aplicație</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/process_details.ui" line="240"/>
        <source>Start or stop monitoring this process</source>
        <translation>Pornește sau oprește monitorizarea acestui proces</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/process_details.ui" line="256"/>
        <source>Close</source>
        <translation>Închide</translation>
    </message>
</context>
<context>
    <name>RulesDialog</name>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="14"/>
        <source>Rule</source>
        <translation>Regulă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="22"/>
        <source>Node</source>
        <translation>Nod</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="45"/>
        <source>Apply rule to all nodes</source>
        <translation>Aplică regula la toate nodurile</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="115"/>
        <source>To this IP / Network</source>
        <translation>Pentru această adresă IP / rețea</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="138"/>
        <source>/path/to/executable, .*/bin/executable[0-9\.]+$, ...</source>
        <translation>/cale/către/executabil, .*/bin/executabil[0-9\.]+$, ...</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="158"/>
        <source>Action</source>
        <translation>Acțiune</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="165"/>
        <source>To this port</source>
        <translation>Pentru acest port</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="172"/>
        <source>To this list of domains</source>
        <translation>Pentru această listă de domenii</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="195"/>
        <source>You can specify a single IP:
- 192.168.1.1

or a regular expression:
- 192\.168\.1\.[0-9]+

multiple IPs:
- ^(192\.168\.1\.1|172\.16\.0\.1)$

You can also specify a subnet:
- 192.168.1.0/24

Note: Commas or spaces are not allowed to separate IPs or networks.</source>
        <translation>You can specify a single IP:
- 192.168.1.1

or a regular expression:
- 192\.168\.1\.[0-9]+

multiple IPs:
- ^(192\.168\.1\.1|172\.16\.0\.1)$

You can also specify a subnet:
- 192.168.1.0/24

Note: Commas or spaces are not allowed to separate IPs or networks.</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="214"/>
        <source>LAN</source>
        <translation>Rețea locală (LAN)</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="219"/>
        <source>127.0.0.0/8</source>
        <translation>127.0.0.0/8</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="224"/>
        <source>192.168.0.0/24</source>
        <translation>192.168.0.0/24</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="229"/>
        <source>192.168.1.0/24</source>
        <translation>192.168.1.0/24</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="234"/>
        <source>192.168.2.0/24</source>
        <translation>192.168.2.0/24</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="239"/>
        <source>192.168.0.0/16</source>
        <translation>192.168.0.0/16</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="244"/>
        <source>169.254.0.0/16</source>
        <translation>169.254.0.0/16</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="249"/>
        <source>172.16.0.0/12</source>
        <translation>172.16.0.0/12</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="254"/>
        <source>10.0.0.0/8</source>
        <translation>10.0.0.0/8</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="259"/>
        <source>::1/128</source>
        <translation>::1/128</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="264"/>
        <source>fc00::/7</source>
        <translation>fc00::/7</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="269"/>
        <source>ff00::/8</source>
        <translation>ff00::/8</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="274"/>
        <source>fe80::/10</source>
        <translation>fe80::/10</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="279"/>
        <source>fd00::/8</source>
        <translation>fd00::/8</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="310"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;You can specify multiple ports using regular expressions:&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;- 53, 80 or 443:&lt;/p&gt;&lt;p&gt;^(53|80|443)$&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;- 53, 443 or 5551, 5552, 5553, etc:&lt;/p&gt;&lt;p&gt;^(53|443|555[0-9])$&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;You can specify multiple ports using regular expressions:&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;- 53, 80 or 443:&lt;/p&gt;&lt;p&gt;^(53|80|443)$&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;- 53, 443 or 5551, 5552, 5553, etc:&lt;/p&gt;&lt;p&gt;^(53|443|555[0-9])$&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="318"/>
        <source>once</source>
        <translation>o dată</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="323"/>
        <source>30s</source>
        <translation>30s</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="328"/>
        <source>5m</source>
        <translation>5m</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="333"/>
        <source>15m</source>
        <translation>15m</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="338"/>
        <source>30m</source>
        <translation>30m</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="343"/>
        <source>1h</source>
        <translation>1o</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="348"/>
        <source>until reboot</source>
        <translation>până la repornire</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="353"/>
        <source>always</source>
        <translation>întotdeauna</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="364"/>
        <source>Commas or spaces are not allowed to specify multiple domains. 

Use regular expressions instead: 
.*(opensnitch|duckduckgo).com
.*\.google.com

or a single domain:
www.gnu.org - it&apos;ll only match www.gnu.org, nor ftp.gnu.org, nor www2.gnu.org, ...
gnu.org         - it&apos;ll only match gnu.org, nor www.gnu.org, nor ftp.gnu.org, ...</source>
        <translation>Commas or spaces are not allowed to specify multiple domains. 

Use regular expressions instead: 
.*(opensnitch|duckduckgo).com
.*\.google.com

or a single domain:
www.gnu.org - it&apos;ll only match www.gnu.org, nor ftp.gnu.org, nor www2.gnu.org, ...
gnu.org         - it&apos;ll only match gnu.org, nor www.gnu.org, nor ftp.gnu.org, ...</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="375"/>
        <source>www.domain.org, .*\.domain.org</source>
        <translation>www.domeniu.org, .*\.domeniu.org</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="382"/>
        <source>To this host</source>
        <translation>Pentru această gazdă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="396"/>
        <source>Duration</source>
        <translation>Durată</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="406"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Only TCP, UDP or UDPLITE are allowed&lt;/p&gt;&lt;p&gt;You can use regexp, i.e.: ^(TCP|UDP)$&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Only TCP, UDP or UDPLITE are allowed&lt;/p&gt;&lt;p&gt;You can use regexp, i.e.: ^(TCP|UDP)$&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="416"/>
        <source>TCP</source>
        <translation>TCP</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="421"/>
        <source>UDP</source>
        <translation>UDP</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="426"/>
        <source>UDPLITE</source>
        <translation>UDPLITE</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="431"/>
        <source>TCP6</source>
        <translation>TCP6</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="436"/>
        <source>UDP6</source>
        <translation>UDP6</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="441"/>
        <source>UDPLITE6</source>
        <translation>UDPLITE6</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="449"/>
        <source>Protocol</source>
        <translation>Protocol</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="456"/>
        <source>From this executable</source>
        <translation>De la acest executabil</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="471"/>
        <source>Deny</source>
        <translation>Refuză</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="494"/>
        <source>Allow</source>
        <translation>Permite</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="507"/>
        <source>From this command line</source>
        <translation>De la această linie de comandă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="514"/>
        <source>From this user ID</source>
        <translation>De la acest ID utilizator</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="539"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select a directory with lists of domains to block or allow.&lt;/p&gt;&lt;p&gt;Put inside that directory files with any extension containing lists of domains.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;The format of each entry of a list is as follow (hosts format):&lt;/p&gt;&lt;p&gt;127.0.0.1 www.domain.com&lt;/p&gt;&lt;p&gt;or &lt;/p&gt;&lt;p&gt;0.0.0.0 www.domain.com&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select a directory with lists of domains to block or allow.&lt;/p&gt;&lt;p&gt;Put inside that directory files with any extension containing lists of domains.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;The format of each entry of a list is as follow (hosts format):&lt;/p&gt;&lt;p&gt;127.0.0.1 www.domain.com&lt;/p&gt;&lt;p&gt;or &lt;/p&gt;&lt;p&gt;0.0.0.0 www.domain.com&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="557"/>
        <source>Name</source>
        <translation>Nume</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="566"/>
        <source>Enable</source>
        <translation>Activează</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="604"/>
        <source>The rules are checked in alphabetical order, so you can name them accordingly to prioritize them.

000-allow-localhost
001-deny-broadcast
...</source>
        <translation>Regulile sunt verificate în ordine alfabetică, așa că puteți să le numiți ca atare pentru a le prioritiza.

000-permite-localhost
001-respinge-broadcast
...</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="611"/>
        <source>leave blank to autocreate</source>
        <translation>lăsați gol pentru creare automată</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="620"/>
        <source>If checked, this rule will take precedence over the rest of the rules. No others rules will be checked after this one.

You must name the rule in such manner that it&apos;ll be checked first, because they&apos;re checked in alphabetical order. For example:

[x] Priority - 000-priority-rule
[  ] Priority - 001-less-priority-rule</source>
        <translation>If checked, this rule will take precedence over the rest of the rules. No others rules will be checked after this one.

You must name the rule in such manner that it&apos;ll be checked first, because they&apos;re checked in alphabetical order. For example:

[x] Priority - 000-priority-rule
[  ] Priority - 001-less-priority-rule</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="628"/>
        <source>Priority rule</source>
        <translation>Regulă prioritate</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="648"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default, the field of the rules are case-insensitive, i.e., if a process tries to access gOOgle.CoM and you have a rule to Deny .*google.com, the connection will be blocked.&lt;br/&gt;&lt;/p&gt;&lt;p&gt;If you check this box, you have to specify the exact string (domain, executable, command line) that you want to filter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;By default, the field of the rules are case-insensitive, i.e., if a process tries to access gOOgle.CoM and you have a rule to Deny .*google.com, the connection will be blocked.&lt;br/&gt;&lt;/p&gt;&lt;p&gt;If you check this box, you have to specify the exact string (domain, executable, command line) that you want to filter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/ruleseditor.ui" line="651"/>
        <source>Case-sensitive</source>
        <translation>Sensibil la majuscule</translation>
    </message>
</context>
<context>
    <name>StatsDialog</name>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="34"/>
        <source>OpenSnitch Network Statistics</source>
        <translation>Statistici de rețea OpenSnitch</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="105"/>
        <source>Save to CSV.</source>
        <translation>Salvează într-un fișier CSV.</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="115"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="166"/>
        <source>Create a new rule</source>
        <translation>Creare regulă nouă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="196"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;hostname - 192.168.1.1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:11pt; font-weight:600;&quot;&gt;hostname - 192.168.1.1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="235"/>
        <source>Status</source>
        <translation>Stare</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="1697"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="279"/>
        <source>Start or Stop interception</source>
        <translation>Porniți sau opriți interceptarea</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="324"/>
        <source>Events</source>
        <translation>Evenimente</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="344"/>
        <source>Filter</source>
        <translation>Filtru</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="357"/>
        <source>Allow</source>
        <translation>Permite</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="366"/>
        <source>Deny</source>
        <translation>Refuză</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="384"/>
        <source>Ex.: firefox</source>
        <translation>De exemplu: firefox</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="411"/>
        <source>50</source>
        <translation>50</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="416"/>
        <source>100</source>
        <translation>100</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="421"/>
        <source>200</source>
        <translation>200</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="426"/>
        <source>300</source>
        <translation>300</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="439"/>
        <source>Delete all intercepted events</source>
        <translation>Șterge toate evenimentele de interceptare</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="825"/>
        <source>Nodes</source>
        <translation>Noduri</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="554"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:7pt;&quot;&gt;(double click on the Addr column to view details of a node)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:7pt;&quot;&gt;(double click on the Addr column to view details of a node)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="1601"/>
        <source>Rules</source>
        <translation>Reguli</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="610"/>
        <source>enable</source>
        <translation>Activează</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="617"/>
        <source>Edit rule</source>
        <translation>Editare regulă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="631"/>
        <source>Delete rule</source>
        <translation>Șterge regula</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="699"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:7pt;&quot;&gt;(double click on a row to view details of a rule)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:7pt;&quot;&gt;(faceți clic dublu pe un rând pentru a vizualiza detaliile regulii)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="692"/>
        <source>search rule name</source>
        <translation>Căutare nume regulă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="781"/>
        <source>Application rules</source>
        <translation>Reguli aplicație</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="796"/>
        <source>Permanent</source>
        <translation type="unfinished">Permanent</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="810"/>
        <source>Temporary</source>
        <translation>Temporar</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="876"/>
        <source>Hosts</source>
        <translation>Gazde</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="1364"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:7pt;&quot;&gt;(double click to view details of an item)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:7pt;&quot;&gt;(faceți clic dublu pentru a vizualiza detaliile unui element)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="984"/>
        <source>Applications</source>
        <translation>Aplicații</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="1051"/>
        <source>Delete all intercepted applications</source>
        <translation>Șterge toate aplicațiile interceptate</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="1109"/>
        <source>Addresses</source>
        <translation>Adrese</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="1211"/>
        <source>Ports</source>
        <translation>Porturi</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="1313"/>
        <source>Users</source>
        <translation>Utilizatori</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="1436"/>
        <source>Connections</source>
        <translation>Conexiuni</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="1491"/>
        <source>Dropped</source>
        <translation>Aruncate</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="1546"/>
        <source>Uptime</source>
        <translation>Durată activitate</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="1671"/>
        <source>Version</source>
        <translation>Versiune</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="665"/>
        <source>Delete connections that matched this rule</source>
        <translation>Șterge toate conexiunile care se potrivesc cu această regulă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="712"/>
        <source>All applications</source>
        <translation>Toate aplicațiile</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="926"/>
        <source>Delete all intercepted hosts</source>
        <translation>Șterge toate gazdele interceptate</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="1159"/>
        <source>Delete all intercepted addresses</source>
        <translation>Șterge toate adresele interceptate</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="1261"/>
        <source>Delete all intercepted ports</source>
        <translation>Șterge toate porturile interceptate</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/res/stats.ui" line="1371"/>
        <source>Delete all intercepted users</source>
        <translation>Șterge toți utilizatorii interceptați</translation>
    </message>
</context>
<context>
    <name>contextual_menu</name>
    <message>
        <location filename="../../../opensnitch/service.py" line="39"/>
        <source>Statistics</source>
        <translation>Statistici</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/service.py" line="40"/>
        <source>Enable</source>
        <translation>Activează</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/service.py" line="41"/>
        <source>Disable</source>
        <translation>Dezactivează</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/service.py" line="42"/>
        <source>Help</source>
        <translation>Ajutor</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/service.py" line="43"/>
        <source>Close</source>
        <translation>Închide</translation>
    </message>
</context>
<context>
    <name>popups</name>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="51"/>
        <source>until reboot</source>
        <translation>până la repornire</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="53"/>
        <source>forever</source>
        <translation>mereu</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="89"/>
        <source>Allow</source>
        <translation>Permite</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="90"/>
        <source>Deny</source>
        <translation>Refuză</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="265"/>
        <source>Outgoing connection</source>
        <translation>Conexiune de ieșire</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="270"/>
        <source>Process launched from:</source>
        <translation>Procesul a fost lansat din:</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="299"/>
        <source>from this executable</source>
        <translation>de la acest executabil</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="301"/>
        <source>from this command line</source>
        <translation>de la această linie de comandă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="305"/>
        <source>to port {0}</source>
        <translation>către portul {0}</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="364"/>
        <source>to {0}</source>
        <translation>către {0}</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="308"/>
        <source>from user {0}</source>
        <translation>de la utilizatorul {0}</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="321"/>
        <source>to {0}.*</source>
        <translation>către {0}.*</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="374"/>
        <source>to *.{0}</source>
        <translation>către *.{0}</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="378"/>
        <source>to *{0}</source>
        <translation>către *{0}</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="411"/>
        <source>&lt;b&gt;Remote&lt;/b&gt; process %s running on &lt;b&gt;%s&lt;/b&gt;</source>
        <translation>Procesul %s&lt;b&gt;telecomandat&lt;/b&gt; rulează pe &lt;b&gt;%s&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="415"/>
        <source>is connecting to &lt;b&gt;%s&lt;/b&gt; on %s port %d</source>
        <translation>se conectează la &lt;b&gt;%s&lt;/b&gt; pe %s portul %d</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/prompt.py" line="421"/>
        <source>is attempting to resolve &lt;b&gt;%s&lt;/b&gt; via %s, %s port %d</source>
        <translation>încearcă să rezolve &lt;b&gt;%s&lt;/b&gt; prin %s, %s portul %d</translation>
    </message>
</context>
<context>
    <name>preferences</name>
    <message>
        <location filename="../../../opensnitch/dialogs/preferences.py" line="260"/>
        <source>Exception saving config: {0}</source>
        <translation>Exception saving config: {0}</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/preferences.py" line="279"/>
        <source>Warning</source>
        <translation>Avertisment</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/preferences.py" line="279"/>
        <source>You must select a file for the database&lt;br&gt;or choose &quot;In memory&quot; type.</source>
        <translation>You must select a file for the database&lt;br&gt;or choose &quot;In memory&quot; type.</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/preferences.py" line="285"/>
        <source>DB type changed</source>
        <translation>Tipul bazei de date a fost schimbat</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/preferences.py" line="285"/>
        <source>Restart the GUI in order effects to take effect</source>
        <translation>Reporniți interfața grafică cu utilizatorul pentru ca modificările să aibă efect</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/preferences.py" line="340"/>
        <source>Applying configuration on {0} ...</source>
        <translation>Se aplică configurația pe {0} ...</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/preferences.py" line="169"/>
        <source>Server address can not be empty</source>
        <translation>Adresa servitorului nu poate fi goală</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/preferences.py" line="199"/>
        <source>Error loading {0} configuration</source>
        <translation>Error loading {0} configuration</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/preferences.py" line="385"/>
        <source>Configuration applied.</source>
        <translation>Configurația a fost aplicată.</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/preferences.py" line="387"/>
        <source>Error applying configuration: {0}</source>
        <translation>Eroare la aplicarea configurației: {0}</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/preferences.py" line="416"/>
        <source>Hover the mouse over the texts to display the help&lt;br&gt;&lt;br&gt;Don&apos;t forget to visit the wiki: &lt;a href=&quot;{0}&quot;&gt;{0}&lt;/a&gt;</source>
        <translation>Hover the mouse over the texts to display the help&lt;br&gt;&lt;br&gt;Don&apos;t forget to visit the wiki: &lt;a href=&quot;{0}&quot;&gt;{0}&lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>proc_details</name>
    <message>
        <location filename="../../../opensnitch/dialogs/processdetails.py" line="96"/>
        <source>&lt;b&gt;Error loading process information:&lt;/b&gt; &lt;br&gt;&lt;br&gt;

</source>
        <translation>&lt;b&gt;Eroare la încărcarea informațiilor procesului:&lt;/b&gt; &lt;br&gt;&lt;br&gt;

</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/processdetails.py" line="115"/>
        <source>&lt;b&gt;Error stopping monitoring process:&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <translation>&lt;b&gt;Eroare la oprirea monitorizării procesului:&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/processdetails.py" line="155"/>
        <source>loading...</source>
        <translation>Se încarcă...</translation>
    </message>
</context>
<context>
    <name>rules</name>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="124"/>
        <source>There&apos;re no nodes connected.</source>
        <translation>Nu există niciun nod conectat.</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="135"/>
        <source>Rule applied.</source>
        <translation>Regulă aplicată.</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="137"/>
        <source>Error applying rule: {0}</source>
        <translation>Eroare la aplicarea regulii: {0}</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="290"/>
        <source>&lt;b&gt;Error loading rule&lt;/b&gt;</source>
        <translation>&lt;b&gt;Eroare la încărcarea regulii&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="395"/>
        <source>protocol can not be empty, or uncheck it</source>
        <translation>Protocolul nu poate fi gol, sau debifați-l</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="409"/>
        <source>Protocol regexp error</source>
        <translation>Protocol regexp error</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="413"/>
        <source>process path can not be empty</source>
        <translation>Calea procesului nu poate fi goală</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="427"/>
        <source>Process path regexp error</source>
        <translation>Process path regexp error</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="431"/>
        <source>command line can not be empty</source>
        <translation>Linia de comandă nu poate fi goală</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="445"/>
        <source>Command line regexp error</source>
        <translation>Command line regexp error</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="449"/>
        <source>Dest port can not be empty</source>
        <translation>Dest port can not be empty</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="463"/>
        <source>Dst port regexp error</source>
        <translation>Dst port regexp error</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="467"/>
        <source>Dest host can not be empty</source>
        <translation>Dest host can not be empty</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="481"/>
        <source>Dst host regexp error</source>
        <translation>Dst host regexp error</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="485"/>
        <source>Dest IP/Network can not be empty</source>
        <translation>Dest IP/Network can not be empty</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="507"/>
        <source>Dst IP regexp error</source>
        <translation>Dst IP regexp error</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="519"/>
        <source>User ID can not be empty</source>
        <translation>User ID can not be empty</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="533"/>
        <source>User ID regexp error</source>
        <translation>User ID regexp error</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="537"/>
        <source>Lists field cannot be empty</source>
        <translation>Lists field cannot be empty</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="539"/>
        <source>Lists field must be a directory</source>
        <translation>Lists field must be a directory</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/ruleseditor.py" line="573"/>
        <source>&lt;b&gt;Rule not supported&lt;/b&gt;</source>
        <translation>&lt;b&gt;Regula nu este sprijinită&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>stats</name>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="284"/>
        <source>Not running</source>
        <translation>Nu rulează</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="285"/>
        <source>Disabled</source>
        <translation>Dezactivată</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="286"/>
        <source>Running</source>
        <translation>Rulează</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="475"/>
        <source>OpenSnitch Network Statistics {0}</source>
        <translation>OpenSnitch Network Statistics {0}</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="477"/>
        <source>OpenSnitch Network Statistics for {0}</source>
        <translation>OpenSnitch Network Statistics for {0}</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="591"/>
        <source>&lt;b&gt;Error:&lt;/b&gt;&lt;br&gt;&lt;br&gt;</source>
        <comment>{0}</comment>
        <translation>&lt;b&gt;Eroare:&lt;/b&gt;&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="598"/>
        <source>Warning:</source>
        <translation>Avertisment:</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="640"/>
        <source>Allow</source>
        <translation>Permite</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="641"/>
        <source>Deny</source>
        <translation>Refuză</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="644"/>
        <source>Always</source>
        <translation>Întotdeauna</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="645"/>
        <source>Until reboot</source>
        <translation>Până la repornire</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="653"/>
        <source>Disable</source>
        <translation>Dezactivează</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="655"/>
        <source>Enable</source>
        <translation>Activează</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="658"/>
        <source>Duplicate</source>
        <translation>Duplică</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="659"/>
        <source>Edit</source>
        <translation>Editare</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="660"/>
        <source>Delete</source>
        <translation>Șterge</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="668"/>
        <source>    Your are about to delete this rule.    </source>
        <translation>    Sunteți pe cale să ștergeți aceasă regulă.    </translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="1003"/>
        <source>    Are you sure?</source>
        <translation>    Sigur doriți acest lucru?</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="786"/>
        <source>Rule not found by that name and node</source>
        <translation>Regula nu a putut fi găsită după acel nume și nod</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="1003"/>
        <source>    You are about to delete this rule.    </source>
        <translation>    Sunteți pe cale să ștergeți această regulă.    </translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="1490"/>
        <source>Save as CSV</source>
        <translation>Save as CSV</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="261"/>
        <source>Name</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Nume</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="262"/>
        <source>Address</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Adresă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="263"/>
        <source>Status</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Stare</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="264"/>
        <source>Hostname</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Nume gazdă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="265"/>
        <source>Version</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Versiune</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="266"/>
        <source>Rules</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Reguli</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="267"/>
        <source>Time</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Timp</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="268"/>
        <source>Action</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Acțiune</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="269"/>
        <source>Duration</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Durată</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="270"/>
        <source>Node</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Nod</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="271"/>
        <source>Enabled</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Activată</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="272"/>
        <source>Hits</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Atingeri</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="273"/>
        <source>Protocol</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Protocol</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="274"/>
        <source>Process</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Proces</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="276"/>
        <source>Destination</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Destinație</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="280"/>
        <source>Rule</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Regulă</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="281"/>
        <source>UserID</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>IdentificatorUtilizator</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="282"/>
        <source>LastConnection</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>UltimaConexiune</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="275"/>
        <source>Args</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>Argumente</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="277"/>
        <source>DstIP</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>DstIP</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="278"/>
        <source>DstHost</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>DstHost</translation>
    </message>
    <message>
        <location filename="../../../opensnitch/dialogs/stats.py" line="279"/>
        <source>DstPort</source>
        <comment>This is a word, without spaces and symbols.</comment>
        <translation>DstPort</translation>
    </message>
</context>
</TS>
