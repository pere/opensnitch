#!/bin/sh
set -e 

/usr/sbin/iptables -t mangle -L OUTPUT | grep "NFQUEUE.*ctstate NEW,RELATED.*NFQUEUE num.*bypass"
echo "[+] Interception rule: OK"
